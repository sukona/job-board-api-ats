class CreateApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :applications do |t|
      t.string :resume
      t.integer :user_id
      t.integer :job_id

      t.timestamps
    end
  end
end
