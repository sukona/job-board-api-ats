Rails.application.routes.draw do
  namespace :v1 do
    resources :sessions
  end
  
  root "home#index"
  
  resources :jobs
  devise_for :users

  get "/jobs", to: "jobs#index"
  get "/applications", to: "applications#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
